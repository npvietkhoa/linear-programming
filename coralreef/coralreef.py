from gurobipy import *


def solve(reefs, corals, population, growth, costs, space,
          target_population, target_population_in_reef, endangered):
    model = Model('cORalreef')

    x = {}
    for r in reefs:
        for c in corals:
            x[r, c] = model.addVar(obj=costs[r, c]/100, name=f'x_{r}_{c}')

    model.modelSense = GRB.MINIMIZE

    # update vars
    model.update()

    # Erste Nebenbedingung:
    # Bedeutung: Pro KORallenriff r soll die gesamte KORallenpopulation nach einem Jahr mindestens target_population_in_reef[r] erreichen
    # Ungleichung: (population[r, c1] * (1 + x_r_c1/100 * growth[c1])
    #            + (population[r, c2] * (1 + x_r_c2/100 * growth[c2])
    #            + ... >= target_population_in_reef[r]
    #            fuer jedes r
    for r in reefs:
        model.addConstr(
            quicksum(population[r, c] * (1 + (x[r, c] / 100) * growth[c]) for c in corals) >= target_population_in_reef[r]
        )

    # Zweite Nebenbedingung:
    # Bedeutung: Pro KORallenarten c soll die Population (in allen KORallenriffen) nach einem Jahr mindestens target_population[c] erreichen
    # Ungleichung: (population[r1, c] * (1 + x_r1_c * growth[c])
    #            + (population[r2, c] * (1 + x_r2_c * growth[c])
    #            + ... >= target_population[c]
    #            fuer jedes c
    for c in corals:
        model.addConstr(quicksum(population[r, c] * (1 + (x[r, c] / 100) * growth[c]) for r in reefs) >= target_population[c])

    # Dritte Nebenbedingung:
    # Bedeutung: Unter den KORallenarten gibt es gefaehrdete Arten, welche in der Liste endangered \subset corals aufgelistet sind.
    # Mindestens 1/3 der zu Schutzgebiet erklaerten Flaeche soll den gefaehrdeten KORallenarten zugeordnet werden
    # Ungleichung: sum of (x[r, c]) fuer alle r und c in endangered >= 1/3 * sum of (x[r, c]) fuer alle r und c
    model.addConstr(
        quicksum(x[r, c] for r in reefs for c in endangered)
        >= 1/3 * quicksum(x[r, c] for r in reefs for c in corals)
    )


    # Vierte Nebenbedingung:
    # Bedeutung: Zudem hat jedes KORallenriff r nur endliche Fläche space[r] in km^2,
    # von der maximal 1/3 zu Schutzgebiet erklaert werden darf.
    # Ungleichung: x_r_c1 + x_r_c2 + ... <= 1/3 * space[r] fuer jedes r und jedes c in endangered
    for r in reefs:
        model.addConstr(quicksum(x[r, c] for c in corals) <= 1/3 * space[r])
    # optimize
    model.optimize()

    # Ausgabe der Loesung.
    if model.status == GRB.OPTIMAL:
        print('\nOptimaler Zielfunktionswert: %g\n' % model.ObjVal)
        for r in reefs:
            for c in corals:
                print(f"KORallenriff {r} allokiert fuer {c} {str(x[r, c].x)} qkm")
            print("\n")
    else:
        print('Keine Optimalloesung gefunden. Status: %i' % (model.status))
    return model
