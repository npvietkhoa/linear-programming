from gurobipy import *

import coralreef

reefs, space, target_population_in_reef = multidict({
    'great_barrier_reef': [680, 2000],
    'ningaloo_reef': [850, 2500],
    'montgomery_reef': [1050, 1900],
})

corals, growth, target_population = multidict({
    'Steinkorallen': [0.34,2200],
    'Weichkorallen': [0.16,1900],
    'Schwarze_Korallen': [0.39,1500],
})

population = {
    ('great_barrier_reef', 'Steinkorallen'): 500,
    ('great_barrier_reef', 'Weichkorallen'): 100,
    ('great_barrier_reef', 'Schwarze_Korallen'): 900,

    ('ningaloo_reef', 'Steinkorallen'): 800,
    ('ningaloo_reef', 'Weichkorallen'): 700,
    ('ningaloo_reef', 'Schwarze_Korallen'): 300,

    ('montgomery_reef', 'Steinkorallen'): 759,
    ('montgomery_reef', 'Weichkorallen'): 800,
    ('montgomery_reef', 'Schwarze_Korallen'): 79,
}

costs = {
    ('great_barrier_reef', 'Steinkorallen'): 500,
    ('great_barrier_reef', 'Weichkorallen'): 1303,
    ('great_barrier_reef', 'Schwarze_Korallen'): 900,

    ('ningaloo_reef', 'Steinkorallen'): 906,
    ('ningaloo_reef', 'Weichkorallen'): 1410,
    ('ningaloo_reef', 'Schwarze_Korallen'): 550,
    
    ('montgomery_reef', 'Steinkorallen'): 200,
    ('montgomery_reef', 'Weichkorallen'): 1400,
    ('montgomery_reef', 'Schwarze_Korallen'): 100,
}

endangered = ['Steinkorallen']

coralreef.solve(reefs, corals, population, growth, costs, space,
             target_population, target_population_in_reef, endangered)
