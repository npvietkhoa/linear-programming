from gurobipy import *

import coralreef

reefs, space, target_population_in_reef = multidict({
    'great_barrier_reef': [980, 4800],
    'ningaloo_reef': [850, 2500],
    'nimontgomery_reef': [1050, 2800],
    'heart_reef': [4000, 4600],
    'flinders_reef': [5800, 3940],
    'rottnest_island': [20150, 16250],
})

corals, growth, target_population = multidict({
    'Steinkoralle': [0.38,4800],
    'Weichkoralle': [0.14,5700],
    'Schwarze_Koralle': [0.35,11500],
    'Feuerkoralle': [0.22,20350],
    'Filigrankoralle': [0.1,2900],
    'Lederkoralle': [0.45,30100],
})

population = {
    ('great_barrier_reef', 'Steinkoralle'): 500,
    ('great_barrier_reef', 'Weichkoralle'): 100,
    ('great_barrier_reef', 'Schwarze_Koralle'): 900,
    ('great_barrier_reef', 'Feuerkoralle'): 900,
    ('great_barrier_reef', 'Filigrankoralle'): 320,
    ('great_barrier_reef', 'Lederkoralle'): 980,

    ('ningaloo_reef', 'Steinkoralle'): 800,
    ('ningaloo_reef', 'Weichkoralle'): 700,
    ('ningaloo_reef', 'Schwarze_Koralle'): 300,
    ('ningaloo_reef', 'Feuerkoralle'): 900,
    ('ningaloo_reef', 'Filigrankoralle'): 830,
    ('ningaloo_reef', 'Lederkoralle'): 1250,

    ('nimontgomery_reef', 'Steinkoralle'): 300,
    ('nimontgomery_reef', 'Weichkoralle'): 800,
    ('nimontgomery_reef', 'Schwarze_Koralle'): 79,
    ('nimontgomery_reef', 'Feuerkoralle'): 3340,
    ('nimontgomery_reef', 'Filigrankoralle'): 467,
    ('nimontgomery_reef', 'Lederkoralle'): 8900,

    ('heart_reef', 'Steinkoralle'): 18,
    ('heart_reef', 'Weichkoralle'): 300,
    ('heart_reef', 'Schwarze_Koralle'): 105,
    ('heart_reef', 'Feuerkoralle'): 53,
    ('heart_reef', 'Filigrankoralle'): 620,
    ('heart_reef', 'Lederkoralle'): 3450,

    ('flinders_reef', 'Steinkoralle'): 567,
    ('flinders_reef', 'Weichkoralle'): 8968,
    ('flinders_reef', 'Schwarze_Koralle'): 1250,
    ('flinders_reef', 'Feuerkoralle'): 3450,
    ('flinders_reef', 'Filigrankoralle'): 355,
    ('flinders_reef', 'Lederkoralle'): 5678,

    ('rottnest_island', 'Steinkoralle'): 76,
    ('rottnest_island', 'Weichkoralle'): 908,
    ('rottnest_island', 'Schwarze_Koralle'): 480,
    ('rottnest_island', 'Feuerkoralle'): 1100,
    ('rottnest_island', 'Filigrankoralle'): 140,
    ('rottnest_island', 'Lederkoralle'): 700,
}

costs = {
    ('great_barrier_reef', 'Steinkoralle'): 600,
    ('great_barrier_reef', 'Weichkoralle'): 1303,
    ('great_barrier_reef', 'Schwarze_Koralle'): 900,
    ('great_barrier_reef', 'Feuerkoralle'): 1200,
    ('great_barrier_reef', 'Filigrankoralle'): 250,
    ('great_barrier_reef', 'Lederkoralle'): 120,

    ('ningaloo_reef', 'Steinkoralle'): 906,
    ('ningaloo_reef', 'Weichkoralle'): 1410,
    ('ningaloo_reef', 'Schwarze_Koralle'): 550,
    ('ningaloo_reef', 'Feuerkoralle'): 300,
    ('ningaloo_reef', 'Filigrankoralle'): 199,
    ('ningaloo_reef', 'Lederkoralle'): 120,

    ('nimontgomery_reef', 'Steinkoralle'): 200,
    ('nimontgomery_reef', 'Weichkoralle'): 1400,
    ('nimontgomery_reef', 'Schwarze_Koralle'): 100,
    ('nimontgomery_reef', 'Feuerkoralle'): 699,
    ('nimontgomery_reef', 'Filigrankoralle'): 500,
    ('nimontgomery_reef', 'Lederkoralle'): 320,

    ('heart_reef', 'Steinkoralle'): 100,
    ('heart_reef', 'Weichkoralle'): 2200,
    ('heart_reef', 'Schwarze_Koralle'): 230,
    ('heart_reef', 'Feuerkoralle'): 910,
    ('heart_reef', 'Filigrankoralle'): 809,
    ('heart_reef', 'Lederkoralle'): 290,

    ('flinders_reef', 'Steinkoralle'): 567,
    ('flinders_reef', 'Weichkoralle'): 1468,
    ('flinders_reef', 'Schwarze_Koralle'): 1450,
    ('flinders_reef', 'Feuerkoralle'): 450,
    ('flinders_reef', 'Filigrankoralle'): 855,
    ('flinders_reef', 'Lederkoralle'): 678,

    ('rottnest_island', 'Steinkoralle'): 870,
    ('rottnest_island', 'Weichkoralle'): 808,
    ('rottnest_island', 'Schwarze_Koralle'): 1480,
    ('rottnest_island', 'Feuerkoralle'): 700,
    ('rottnest_island', 'Filigrankoralle'): 470,
    ('rottnest_island', 'Lederkoralle'): 440,
}

endangered = ['Steinkoralle','Filigrankoralle']

coralreef.solve(reefs, corals, population, growth, costs, space,
             target_population, target_population_in_reef, endangered)