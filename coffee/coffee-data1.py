from gurobipy import *
import coffee
farmers = ['Anais', 'Bernd', 'Carl', 'Dora']
commodities = ['Arabica', 'Robusta', 'Liberica', 'Excelsa']
wholesalers = ['Maestle', 'Blagner', 'Dr. Noergler', 'Verreror']

fixed_costs = {(f, c): 0 for f in farmers for c in commodities}
unit_costs = {(f, c): 0 for f in farmers for c in commodities}
availability = {(f, c): 0 for f in farmers for c in commodities}
transport_costs = {(c, f, w): 0 for c in commodities for f in farmers for w in wholesalers}

minimum_demand = {(w, c): 0 for w in wholesalers for c in commodities}
maximum_demand = {(w, c): 0 for w in wholesalers for c in commodities}
price = {(w, c): 0 for w in wholesalers for c in commodities}

fixed_costs[('Anais', 'Arabica')] = 300
fixed_costs[('Anais', 'Robusta')] = 150
fixed_costs[('Bernd', 'Robusta')] = 160
fixed_costs[('Bernd', 'Liberica')] = 400
fixed_costs[('Carl', 'Liberica')] = 300
fixed_costs[('Carl', 'Excelsa')] = 250
fixed_costs[('Dora', 'Arabica')] = 200
fixed_costs[('Dora', 'Excelsa')] = 250

unit_costs[('Anais', 'Arabica')] = 6
unit_costs[('Anais', 'Robusta')] = 4
unit_costs[('Bernd', 'Robusta')] = 3.5
unit_costs[('Bernd', 'Liberica')] = 2
unit_costs[('Carl', 'Liberica')] = 3
unit_costs[('Carl', 'Excelsa')] = 4
unit_costs[('Dora', 'Arabica')] = 3
unit_costs[('Dora', 'Excelsa')] = 1

availability[('Anais', 'Arabica')] = 45
availability[('Anais', 'Robusta')] = 30
availability[('Bernd', 'Robusta')] = 60
availability[('Bernd', 'Liberica')] = 70
availability[('Carl', 'Liberica')] = 80
availability[('Carl', 'Excelsa')] = 65
availability[('Dora', 'Arabica')] = 85
availability[('Dora', 'Excelsa')] = 50

transport_costs[('Arabica', 'Anais', 'Maestle')] = 3
transport_costs[('Arabica', 'Anais', 'Blagner')] = 2
transport_costs[('Arabica', 'Anais', 'Dr. Noergler')] = 4
transport_costs[('Arabica', 'Anais', 'Verreror')] = 1
transport_costs[('Arabica', 'Dora', 'Maestle')] = 3
transport_costs[('Arabica', 'Dora', 'Blagner')] = 4
transport_costs[('Arabica', 'Dora', 'Dr. Noergler')] = 1
transport_costs[('Arabica', 'Dora', 'Verreror')] = 3
transport_costs[('Robusta', 'Anais', 'Maestle')] = 5
transport_costs[('Robusta', 'Anais', 'Blagner')] = 3
transport_costs[('Robusta', 'Anais', 'Dr. Noergler')] = 2
transport_costs[('Robusta', 'Anais', 'Verreror')] = 2
transport_costs[('Robusta', 'Bernd', 'Maestle')] = 6
transport_costs[('Robusta', 'Bernd', 'Blagner')] = 2
transport_costs[('Robusta', 'Bernd', 'Dr. Noergler')] = 3
transport_costs[('Robusta', 'Bernd', 'Verreror')] = 1
transport_costs[('Liberica', 'Bernd', 'Maestle')] = 2
transport_costs[('Liberica', 'Bernd', 'Blagner')] = 3
transport_costs[('Liberica', 'Bernd', 'Dr. Noergler')] = 5
transport_costs[('Liberica', 'Bernd', 'Verreror')] = 2
transport_costs[('Liberica', 'Carl', 'Maestle')] = 4
transport_costs[('Liberica', 'Carl', 'Blagner')] = 2
transport_costs[('Liberica', 'Carl', 'Dr. Noergler')] = 6
transport_costs[('Liberica', 'Carl', 'Verreror')] = 1
transport_costs[('Excelsa', 'Carl', 'Maestle')] = 4
transport_costs[('Excelsa', 'Carl', 'Blagner')] = 2
transport_costs[('Excelsa', 'Carl', 'Dr. Noergler')] = 6
transport_costs[('Excelsa', 'Carl', 'Verreror')] = 1
transport_costs[('Excelsa', 'Dora', 'Maestle')] = 3
transport_costs[('Excelsa', 'Dora', 'Blagner')] = 1
transport_costs[('Excelsa', 'Dora', 'Dr. Noergler')] = 1
transport_costs[('Excelsa', 'Dora', 'Verreror')] = 4

minimum_demand[('Maestle', 'Arabica')] = 5
minimum_demand[('Maestle', 'Robusta')] = 10
minimum_demand[('Verreror', 'Liberica')] = 5
minimum_demand[('Verreror', 'Robusta')] = 10
minimum_demand[('Verreror', 'Arabica')] = 15
minimum_demand[('Dr. Noergler', 'Excelsa')] = 15
minimum_demand[('Dr. Noergler', 'Liberica')] = 5
minimum_demand[('Blagner', 'Excelsa')] = 10
minimum_demand[('Blagner', 'Arabica')] = 5
minimum_demand[('Blagner', 'Liberica')] = 15

maximum_demand[('Maestle', 'Arabica')] = 20
maximum_demand[('Maestle', 'Robusta')] = 10
maximum_demand[('Verreror', 'Liberica')] = 15
maximum_demand[('Verreror', 'Robusta')] = 10
maximum_demand[('Verreror', 'Arabica')] = 20
maximum_demand[('Dr. Noergler', 'Excelsa')] = 30
maximum_demand[('Dr. Noergler', 'Liberica')] = 15
maximum_demand[('Blagner', 'Excelsa')] = 25
maximum_demand[('Blagner', 'Arabica')] = 20
maximum_demand[('Blagner', 'Liberica')] = 25

price[('Maestle', 'Arabica')] = 33
price[('Maestle', 'Robusta')] = 36
price[('Verreror', 'Liberica')] = 23
price[('Verreror', 'Robusta')] = 66
price[('Verreror', 'Arabica')] = 43
price[('Dr. Noergler', 'Excelsa')] = 23
price[('Dr. Noergler', 'Liberica')] = 24
price[('Blagner', 'Excelsa')] = 48
price[('Blagner', 'Arabica')] = 39
price[('Blagner', 'Liberica')] = 54

model = coffee.solve(farmers, commodities, wholesalers, fixed_costs, unit_costs,
                    availability, transport_costs, minimum_demand, maximum_demand, price)
