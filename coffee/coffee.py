from gurobipy import *


def solve(farmers, commodities, wholesalers, fixed_costs, unit_costs, availability, transport_costs, minimum_demand,
          maximum_demand, price):
    # Gurobi-Modell erzeugen.
    model = Model("coffee")

    # Modell ist ein Maximize
    model.modelSense = GRB.MAXIMIZE

    # Variablen-Dictionary fuer den Kauf anlegen.
    x = {}
    # Variablen in Gurobi erzeugen und hinzufuegen.
    for c in commodities:
        for f in farmers:
            for w in wholesalers:
                x[c, f, w] = model.addVar(name=f"x_{c}_{f}_{w}", lb=0)

    # Zielfunktion:
    # Teil der Zielfunktion (benötigte Variablen):
    #   Umsatz durch die von jedem KaffeebauORn verkauften Güter
    # - Fixkosten jedes KaffeebauORn
    # - Transportkosten, welche durch den Verkauf an den Großhändler entstehen für jeden KaffeebauORn
    # - (Stückkosten, die für jede gelieferte Einheit Rohstoff für jeden KaffeebauORn anfallen
    bilanz = 0
    for f in farmers:
        for c in commodities:
            bilanz += quicksum(x[c, f, w] * (price[w, c] - unit_costs[f, c] - transport_costs[c, f, w]) for w in wholesalers)
            bilanz -= fixed_costs[f, c]

    model.setObjective(bilanz)

    # Variablen bekannt machen.
    model.update()

    # Erste Nebenbedingung: Bedeutung: Jeder KaffeebauOR soll für sich betrachtet Gewinn machen (Unterschied zur
    # Zielfunktion: dort soll die Summe aller 'Bilanzen' aller KaffeebauORn zusammen möglichst groß sein)
    # Ungleichung: "..

    for f in farmers:
        umsatz = 0
        for c in commodities:
            umsatz += quicksum(x[c, f, w] * (price[w, c] - unit_costs[f, c] - transport_costs[c, f, w]) for w in wholesalers)
            umsatz -= fixed_costs[f, c]
        model.addConstr(umsatz >= 0)

    # Zweite Nebenbedingung (falls nötig): Bedeutung: Jeder KaffeebauOR f stellt zudem nicht jedes Gut her und hat
    # nur eine maximale Verfügbarkeit availability[f, c] für jedes Gut c
    # Ungleichung: ..

    for f in farmers:
        for c in commodities:
            model.addConstr(quicksum(x[c, f, w] for w in wholesalers) <= availability[f, c])

    # Dritte Nebenbedingung (falls nötig):
    # Bedeutung: Ein Grosshaendler w hat fuer die verschiedenen Gueter c einen Maximalbedarf maximum_demand[w, c]
    # Ungleichung: ...

    for w in wholesalers:
        for c in commodities:
            model.addConstr(quicksum(x[c, f, w] for f in farmers) <= maximum_demand[w, c])

    # Vierte Nebenbedingung  (falls nötig):
    # Bedeutung: Ein Grosshaendler w hat fuer die verschiedenen Gueter c einen Mindestbedarf minimum_demand[w, c]
    # Ungleichung: ...

    for w in wholesalers:
        for c in commodities:
            model.addConstr(quicksum(x[c, f, w] for f in farmers) >= minimum_demand[w, c])

    # Fuenfte Nebenbedingung  (falls nötig):
    # Bedeutung: ...
    # Ungleichung: ...

    # Problem loesen lassen.
    model.optimize()

    # Ausgabe der Loesung.
    if model.status == GRB.OPTIMAL:
        print(f"\nOptimaler Zielfunktionswert: {model.ObjVal:.2f}\n")
        for c in commodities:
            for f in farmers:
                for w in wholesalers:
                    if (x[c, f, w].x > 0):
                        print(
                            f"Es werden {x[c, f, w].x} Mengeneinheiten {c} von KaffeebauOR {f} an Großhändler {w} geschickt.")
    else:
        print(f"Keine Optimalloesung gefunden. Status {model.status}")

    return model
