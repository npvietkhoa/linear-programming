from gurobipy import *
import coffee

farmers = ['Anais', 'Bernd']
commodities = ['Arabica', 'Robusta']
wholesalers = ['Maestle', 'Blagner']

fixed_costs = {(f, c): 0 for f in farmers for c in commodities}
unit_costs = {(f, c): 0 for f in farmers for c in commodities}
availability = {(f, c): 0 for f in farmers for c in commodities}
transport_costs = {
    (c, f, w): 0 for c in commodities for f in farmers for w in wholesalers}

minimum_demand = {(w, c): 0 for w in wholesalers for c in commodities}
maximum_demand = {(w, c): 0 for w in wholesalers for c in commodities}
price = {(w, c): 0 for w in wholesalers for c in commodities}

fixed_costs[('Anais', 'Arabica')] = 100
fixed_costs[('Anais', 'Robusta')] = 100
fixed_costs[('Bernd', 'Robusta')] = 100

unit_costs[('Anais', 'Arabica')] = 6
unit_costs[('Anais', 'Robusta')] = 4
unit_costs[('Bernd', 'Robusta')] = 3.5

availability[('Anais', 'Arabica')] = 15
availability[('Anais', 'Robusta')] = 20
availability[('Bernd', 'Robusta')] = 10

transport_costs[('Arabica', 'Anais', 'Maestle')] = 3
transport_costs[('Arabica', 'Anais', 'Blagner')] = 2
transport_costs[('Robusta', 'Anais', 'Maestle')] = 5
transport_costs[('Robusta', 'Bernd', 'Maestle')] = 6

minimum_demand[('Maestle', 'Arabica')] = 5
minimum_demand[('Maestle', 'Robusta')] = 10
minimum_demand[('Blagner', 'Arabica')] = 5

maximum_demand[('Maestle', 'Arabica')] = 10
maximum_demand[('Maestle', 'Robusta')] = 15
maximum_demand[('Blagner', 'Arabica')] = 5

price[('Maestle', 'Arabica')] = 50
price[('Maestle', 'Robusta')] = 50
price[('Blagner', 'Arabica')] = 50


model = coffee.solve(farmers, commodities, wholesalers, fixed_costs, unit_costs,
                    availability, transport_costs, minimum_demand, maximum_demand, price)
